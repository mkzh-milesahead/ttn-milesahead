FROM node:10

WORKDIR /opt/ttn-milesahead

# Install dependencies
COPY package.json yarn.lock ./
RUN yarn install

# Copy code
COPY bin ./bin
COPY src ./src

# Build website
COPY web ./web
RUN cd web && yarn install && yarn run build

ENTRYPOINT []
CMD node --experimental-modules bin/index.mjs \
  "--app-id=${TTN_APP_ID}" \
  "--app-key=${TTN_ACCESS_KEY}" \
  "--mongo=${MONGODB_URI}" \
  "--port=${PORT}"
