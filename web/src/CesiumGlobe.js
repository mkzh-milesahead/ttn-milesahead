import Promise from 'promise'
import chroma from 'chroma-js'

class CesiumGlobe {

  async setup (elementId) {
    const Cesium = await import(/* webpackChunkName: "cesium" */ 'cesium/Cesium')

    Cesium.Camera.DEFAULT_VIEW_RECTANGLE = Cesium.Rectangle.fromDegrees(8.5, 47.35, 8.55, 47.4)
    Cesium.Camera.DEFAULT_VIEW_FACTOR = 0

    const viewer = new Cesium.Viewer(elementId, {
      selectionIndicator: false,
      baseLayerPicker: false,
      fullscreenButton: false,
      sceneModePicker: false,
      timeline: false,
      animation: false,
      imageryProvider: Cesium.createOpenStreetMapImageryProvider({
        url: 'https://stamen-tiles.a.ssl.fastly.net/toner/',
        credit: 'Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under CC BY SA.'
      }),
      terrainProvider: Cesium.createWorldTerrain({
        requestWaterMask: true, // required for water effects
        requestVertexNormals: true // required for terrain lighting
      }),
    })
    //viewer.scene.globe.enableLighting = true;
    viewer.scene.globe.depthTestAgainstTerrain = true

    // Add tileset
    const tileset = new Cesium.Cesium3DTileset({
      url: './data/3dtiles/zurich-lod2/tileset.json',
    })
    const city = viewer.scene.primitives.add(tileset)

    // Adjust the tileset height
    const heightOffset = +50
    city.readyPromise.then(function (tileset) {
      const boundingSphere = tileset.boundingSphere
      const cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center)
      const surfacePosition = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0)
      const offsetPosition = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset)
      const translation = Cesium.Cartesian3.subtract(offsetPosition, surfacePosition, new Cesium.Cartesian3())
      tileset.modelMatrix = Cesium.Matrix4.fromTranslation(translation)
    })

    // Zoom to tileset once everything is loaded
    Promise.all([
      tileset.readyPromise,
      viewer.terrainProvider.readyPromise
    ]).then(() => {
      viewer.zoomTo(tileset, new Cesium.HeadingPitchRange(0.0, -1.7, tileset.boundingSphere.radius / 4))
    })

    this._viewer = viewer
    window.cesium = viewer
  }

  /**
   * @param {Number} lat
   * @param {Number} lon
   * @param {Number} value 0-1
   */
  async addMarker (lat, lon, value) {
    const Cesium = await import(/* webpackChunkName: "cesium" */ 'cesium/Cesium')

    // this._viewer.entities.add({
    //   position: Cesium.Cartesian3.fromDegrees(lon, lat, 0),
    //   point: {
    //     color: Cesium.Color.RED,
    //     outlineWidth: 20,
    //     pixelSize: 20,
    //     heightReference: Cesium.HeightReference.CLAMP_TO_GROUND,
    //   }
    // })

    let colorScale = chroma.scale('RdYlBu').domain([1, 0])
    let color = colorScale(value)
    let colorRgb = color.rgb().map((v) => v / 255)
    let geometryInstance = new Cesium.GeometryInstance({
      geometry: new Cesium.CircleGeometry({
        center: Cesium.Cartesian3.fromDegrees(lon, lat),
        radius: 10.0,
      }),
      attributes: {
        color: new Cesium.ColorGeometryInstanceAttribute(...colorRgb, 1.0)
      }
    })
    this._viewer.scene.primitives.add(new Cesium.GroundPrimitive({
      geometryInstances: geometryInstance
    }))

  }
}

export default CesiumGlobe
