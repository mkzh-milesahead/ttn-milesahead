#!/usr/bin/node --experimental-modules

import caporal from 'caporal'
import Database from '../src/Database'
import TTNApplication from '../src/TTNApplication'
import WebServer from '../src/WebServer'

caporal
  .option('--app-id <app-id>', 'TTN Application ID', null, null, true)
  .option('--app-key <app-key>', 'TTN Application Access Key', null, null, true)
  .option('--mongo <mongodb-uri>', 'MongoDB connection URI', null, null, true)
  .option('--port <port>', 'Port for the webserver to listen on', null, 8080)
  .action(async function (args, {appId, appKey, mongo, port}, logger) {
    let db = new Database(mongo, logger)
    let ttnApp = new TTNApplication(appId, appKey, db, logger)
    let webServer = new WebServer(port, db, logger)
    await ttnApp.run()
    await webServer.run()
  })

caporal.parse(process.argv)
