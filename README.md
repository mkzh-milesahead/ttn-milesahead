ttn-milesahead
==============

[TTN](https://www.thethingsnetwork.org/) application for team *Miles Ahead*.

The application will listen for TTN messages (see "Payload Format" below), and store them in MongoDB.
A web server is listening on port 8080, serving a static website and a REST API to retrieve the data (http://localhost:8080).

Payload Format
--------------

Uplink message:
- **lat**: 3 bytes `((latitude + 90) / 180) * 256^3`
- **lon**: 3 bytes `((latitude + 180) / 360) * 256^3`
- **temp-inside**: 2 bytes `((celsius + 50) / 100) * 256^2`
- **temp-outside**: 2 bytes `((celsius + 50) / 100) * 256^2`
- **tiltx**: 1 byte `((tiltx + 2) / 4) * 256^1`
- **tilty**: 1 byte `((tilty + 2) / 4) * 256^1`
- **speed**: 2 bytes `(m/s / 99) * 256^2`
- **dist**: 2 bytes `(m / 9999) * 256^2`

For example `(47.3758433, 8.5129272, 8, 20, 0, 0.5, 1, 100)` would become in Hex:
```
C36104 860DBB 947A B333 80 A0 0295 028F
C36104860DBB947AB33380A00295028F
```

Local Development
-----------------

Install dependencies:
```
yarn install
```

Install website dependencies:
```
(cd web && yarn install && yarn run build)
```

Run:
```
node --experimental-modules index.mjs
```

TTN CLI
-------

Connect to the app with [ttnctl](https://www.thethingsnetwork.org/docs/network/cli/quick-start.html):
```
ttnctl applications select ttn-milesahead
```

Create a test device:
```
ttnctl devices register test-dev-1
```

Simulate an uplink message "01FF" (511) on port "1":
```
ttnctl devices simulate test-dev-1 --port 1 'C36104860DBB947AB333'
```

Heroku Deployment
-----------------

App is deployed to Heroku via Gitlab CI:
https://dashboard.heroku.com/apps/ttn-milesahead

### Setup Local Environment

Login to Heroku:
```
heroku login
```

Add a Git remote to link the folder to the  Heroku app:
```
heroku git:remote -a ttn-milesahead
```
