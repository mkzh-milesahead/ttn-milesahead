import ttn from 'ttn'
import fs from 'fs'

class TTNApplication {

  /**
   * @param {String} appId
   * @param {String} accessKey
   * @param {Database} db
   * @param logger
   */
  constructor (appId, accessKey, db, logger) {
    this._logger = logger
    this._appId = appId
    this._accessKey = accessKey
    this._db = db
  }

  async run () {
    let app = await ttn.application(this._appId, this._accessKey)
    await app.setCustomPayloadFunctions({
      decoder: fs.readFileSync('src/payload-functions/UplinkDecoder.js', 'utf8'),
      converter: fs.readFileSync('src/payload-functions/UplinkConverter.js', 'utf8'),
      validator: fs.readFileSync('src/payload-functions/UplinkValidator.js', 'utf8'),
      encoder: fs.readFileSync('src/payload-functions/DownlinkEncoder.js', 'utf8'),
    })

    this._ttnClient = await ttn.data(this._appId, this._accessKey)
    this._ttnClient.on('uplink', async (deviceId, data) => {
      await this._handleMessage(deviceId, data['port'], data['payload_fields'], data['metadata'])
    })

    this._logger.info(`TTN application "${this._appId}" running.`)
  }

  /**
   * @param {String} deviceId
   * @param {Number} port
   * @param {Object} payload
   * @param {Object} metadata
   * @private
   */
  async _handleMessage (deviceId, port, payload, metadata) {
    this._logger.info('Received: ', {
      port: port,
      payload: payload
    })
    await this._db.addRecord(deviceId, payload, metadata)
  }

  /**
   *
   * @param {String} deviceId
   * @param {Object} payload
   * @private
   */
  _sendMessage (deviceId, payload) {
    this._ttnClient.send(deviceId, payload)
  }

}

export default TTNApplication
