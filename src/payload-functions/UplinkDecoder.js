function Decoder (bytes, port) {
  var decoded = {}
  if (port === 1) {
    if (bytes.length === 16) {
      var lat = bytesToInt(bytes.slice(0, 3)) / Math.pow(256, 3) * 180 - 90
      var lon = bytesToInt(bytes.slice(3, 6)) / Math.pow(256, 3) * 360 - 180
      if (lat !== 0 && lon !== 0) {
        decoded['lat'] = lat
        decoded['lon'] = lon
      }

      var tempInside = bytesToInt(bytes.slice(6, 8)) / Math.pow(256, 2) * 100 - 50
      if (tempInside !== -50) {
        decoded['temp-inside'] = tempInside
      }

      var tempOutside = bytesToInt(bytes.slice(8, 10)) / Math.pow(256, 2) * 100 - 50
      if (tempOutside !== -50) {
        decoded['temp-outside'] = tempOutside
      }

      var tiltx = bytesToInt(bytes.slice(10, 11)) / Math.pow(256, 1) * 4 - 2
      decoded['tiltx'] = tiltx
      var tilty = bytesToInt(bytes.slice(11, 12)) / Math.pow(256, 1) * 4 - 2
      decoded['tilty'] = tilty

      var speed = bytesToInt(bytes.slice(12, 14)) / Math.pow(256, 2) * 99
      if (speed !== 99) {
        decoded['speed'] = speed
      }

      var dist = bytesToInt(bytes.slice(14, 16)) / Math.pow(256, 2) * 9999
      if (dist !== 9999) {
        decoded['dist'] = dist
      }
    }
  }
  return decoded
}

function bytesToInt (bytes) {
  var integer = 0
  for (var n = 0; n < bytes.length; n++) {
    integer += bytes[n]
    if (n < bytes.length - 1) {
      integer = integer << 8
    }
  }
  return integer
}
